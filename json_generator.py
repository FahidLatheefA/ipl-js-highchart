"""
Generates json files required for plots
"""


##############################
# Importing Required Packages
##############################

from csv_reader import read_csvfile
from tasks import task1
from tasks import task2
from tasks import task3
from tasks import task4


###############################
# Reading and storing the file
###############################

# Using the created csv_reader function to read the files
# details=True prints number of columns and rows as well
# details=False is the default value which we are using here
deliveries_fields, deliveries_rows = read_csvfile("data/deliveries.csv")
matches_fields, matches_rows = read_csvfile("data/matches.csv")

# Creating column indexes to the datasets
deliveries_col_idx = {val: idx for idx, val in enumerate(deliveries_fields)}
matches_col_idx = {val: idx for idx, val in enumerate(matches_fields)}

print('\n', end="")  # Printing an empty line

if __name__ == "__main__":
    task1.create_json_task1(deliveries_rows, deliveries_col_idx)
    task2.create_json_task2(deliveries_rows, deliveries_col_idx)
    task3.create_json_task3(matches_rows, matches_col_idx)
    task4.create_json_task4(matches_rows, matches_col_idx)
    