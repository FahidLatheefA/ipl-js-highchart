"""
This module contains the function read_csvfile which reads csv files
Used csv.reader to create this function
Helps in reducing length of code in the project.py file
"""

import csv


def read_csvfile(path_to_file, seperator=",", details=False):
    """Read a comma-separated values (csv) file

    Parameters:
        path_to_file: str
            File path in string format
        seperator: str, default ','
            The delimeter used for reading the csv. Default value is comma(,)
        details: bool, default False
            Prints number of rows and number of fields

    Returns: fields and rows
        fields : Field names in list format
        rows : All the rows
    """

    fields = []
    rows = []

    path_to_file = str(path_to_file)

    try:
        # Reading the data from the csv file
        with open(path_to_file, 'r') as csvfile:
            # creating a csv reader iterable object
            csvreader = csv.reader(csvfile, delimiter=seperator)

            # extracting field names through first row
            fields = next(csvreader)

            # extracting each data row one by one
            for row in csvreader:
                rows.append(row)

    except FileNotFoundError:
        print(f"\n'{path_to_file}', Invalid path!\n")
        return [], []

    if details:
        # get total number of rows
        print(f"\nFile '{path_to_file}' read succesfully")
        print("Total no. of rows: %d" % (len(rows)))
        print("Total no. of fields: %d" % (len(fields)))
    else:
        print(f"\nFile '{path_to_file}' read succesfully")

    return fields, rows
