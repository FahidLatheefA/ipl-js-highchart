[Application Link in Heroku](https://ipl-project-js.herokuapp.com)

## About The Project - IPL-JS-HighCharts

This project is created as part of MountBlue Training (Cohort 17 - Python). The project visualizes 4 different elements of the IPL datasets in bar plot format. The plotting is done using Highcharts Library in JavaScript.

## Setup

1)&nbsp;Clone the project

- Clone with SSH

``` shell
git clone git@gitlab.com:mountblue/cohort-17-python/fahid/ipl-js
```

- Clone with HTTPS

``` shell
git clone https://gitlab.com/mountblue/cohort-17-python/fahid/ipl-js
```

2)&nbsp;Setting up the data files

- Download the 2 csv files from this [link](https://www.kaggle.com/manasgarg/ipl/version/5).
- Create a new directory `data` and move the 2 csv files to `data/` directory.

3)&nbsp;Run the json file generator

- Make a new directory json/

```shell
mkdir json
```

- Run the `json_generator.py` to generate json files

```shell
python3 json_generator.py
```

4)&nbsp;Run the index.html with any browser

Voila, the setup has been complete. You can find the graphs in the localhost server.

```
Date: September 9, 2021

Author: Fahid Latheef A
```