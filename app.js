// Plot Task 1a

// Create Toggler
const first = document.getElementById("wrapper1a");

const second = document.getElementById("wrapper1b");

const third = document.getElementById("wrapper1c");

first.querySelector("a").addEventListener("click", () => {
    first.style.display = "none";
    second.style.display = "block";
    third.style.display = "none";
})

second.querySelector("a").addEventListener("click", () => {
    second.style.display = "none";
    third.style.display = "block";
    first.style.display = "none";
})

third.querySelector("a").addEventListener("click", () => {

    third.style.display = "none";
    first.style.display = "block";
    second.style.display = "none";
})


function fetchAndVisualizeData1a() {
    fetch(`json/task1.json`)
        .then(r => r.json())
        .then(data => {
            visualizeData1a(data);
        })
}



function visualizeData1a(data) {


    // Create the chart
    Highcharts.chart('container1a', {
        chart: {
            type: 'column',
        },
        title: {
            text: 'Runs scored by IPL teams: Default Order of teams'
        },
        xAxis: {
            categories: Object.keys(data),
            title: {
                text: 'Teams',
                minPadding: 0,
                maxPadding: 0
            }
        },
        yAxis: {
            title: {
                text: 'Total Runs Scored'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> runs<br/>'
        },

        series: [{
            data: Object.values(data)

        }]
    });

}

fetchAndVisualizeData1a();


// Plot Task 1b


function fetchAndVisualizeData1b() {
    fetch(`json/task1.json`)
        .then(r => r.json())
        .then(data => {
            visualizeData1b(data);
        })
}



function visualizeData1b(data) {

    //Restructuring the data
    data = Object.assign( // collect all objects into a single obj
        ...Object // spread the final array as parameters
        .entries(data) // key a list of key/ value pairs
        .sort(({
            1: a
        }, {
            1: b
        }) => b - a) // sort DESC by index 1
        // .slice(0, 3)                         // get first three items of array
        .map(([k, v]) => ({
            [k]: v
        })) // map an object with a destructured key/value pair
    );

    // Create the chart
    Highcharts.chart('container1b', {
        chart: {
            type: 'column',
        },
        title: {
            text: 'Runs scored by IPL teams: Descending Order of teams'
        },
        xAxis: {
            categories: Object.keys(data),
            title: {
                text: 'Teams'
            }
        },
        yAxis: {
            title: {
                text: 'Total Runs Scored'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> runs<br/>'
        },

        series: [{
            data: Object.values(data)

        }]
    });

}



fetchAndVisualizeData1b();


// Plot Task 1c


function fetchAndVisualizeData1c() {
    fetch(`json/task1.json`)
        .then(r => r.json())
        .then(data => {
            visualizeData1c(data);
        })
}



function visualizeData1c(data) {

    //Restructuring the data
    data = Object.assign( // collect all objects into a single obj
        ...Object // spread the final array as parameters
        .entries(data) // key a list of key/ value pairs
        .sort(({
            1: a
        }, {
            1: b
        }) => a - b) // sort ASC by index 1
        // .slice(0, 3)                         // get first three items of array
        .map(([k, v]) => ({
            [k]: v
        })) // map an object with a destructured key/value pair
    );
    // Create the chart
    Highcharts.chart('container1c', {
        chart: {
            type: 'column',
        },
        title: {
            text: 'Runs scored by IPL teams: Ascending Order of teams'
        },
        xAxis: {
            categories: Object.keys(data),
            title: {
                text: 'Teams'
            }
        },
        yAxis: {
            title: {
                text: 'Total Runs Scored'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> runs<br/>'
        },

        series: [{
            data: Object.values(data)

        }]
    });

}



fetchAndVisualizeData1c();

// Plot Task 2


const dataRCB = {
    "CH Gayle": 3174,
    "Mandeep Singh": 345,
    "TM Head": 203,
    "KM Jadhav": 309,
    "SR Watson": 249,
    "Sachin Baby": 134,
    "STR Binny": 140,
    "S Aravind": 59,
    "YS Chahal": 15,
    "TS Mills": 8,
    "A Choudhary": 19,
    "Vishnu Vinod": 18,
    "Iqbal Abdulla": 62,
    "P Negi": 144,
    "AB de Villiers": 2811,
    "V Kohli": 4423,
    "AF Milne": 6,
    "S Badree": 13,
    "R Dravid": 897,
    "W Jaffer": 130,
    "JH Kallis": 1131,
    "CL White": 162,
    "MV Boucher": 388,
    "B Akhil": 63,
    "AA Noffke": 9,
    "P Kumar": 181,
    "Z Khan": 67,
    "SB Joshi": 5,
    "S Chanderpaul": 25,
    "LRPL Taylor": 516,
    "R Vinay Kumar": 236,
    "B Chipli": 19,
    "DW Steyn": 14,
    "Misbah-ul-Haq": 117,
    "DT Patil": 9,
    "A Kumble": 35,
    "J Arunkumar": 23,
    "SP Goswami": 142,
    "Abdur Razzak": 0,
    "JD Ryder": 56,
    "KP Pietersen": 329,
    "RV Uthappa": 549,
    "R Bishnoi": 19,
    "KV Sharma": 0,
    "Pankaj Singh": 2,
    "RE van der Merwe": 123,
    "MK Pandey": 416,
    "KP Appanna": 2,
    "D du Preez": 9,
    "EJG Morgan": 35,
    "A Mithun": 34,
    "S Sriram": 26,
    "MA Agarwal": 432,
    "TM Dilshan": 587,
    "SS Tiwary": 487,
    "AUK Pathan": 35,
    "CA Pujara": 143,
    "JJ van der Wath": 18,
    "R Ninan": 2,
    "DL Vettori": 53,
    "J Syed Mohammad": 29,
    "M Kaif": 53,
    "LA Pomersbach": 47,
    "KB Arun Karthik": 47,
    "CK Langeveldt": 8,
    "AB McDonald": 47,
    "HV Patel": 47,
    "RR Bhatkal": 0,
    "M Muralitharan": 6,
    "P Parameswaran": 0,
    "DT Christian": 6,
    "KK Nair": 9,
    "MC Henriques": 165,
    "M Kartik": 1,
    "JD Unadkat": 1,
    "R Rampaul": 51,
    "KL Rahul": 417,
    "A Mukund": 18,
    "PA Patel": 205,
    "NJ Maddinson": 16,
    "Yuvraj Singh": 375,
    "YV Takawale": 104,
    "JA Morkel": 44,
    "S Rana": 72,
    "MA Starc": 94,
    "AB Dinda": 4,
    "VR Aaron": 41,
    "RR Rossouw": 53,
    "VH Zol": 29,
    "AN Ahmed": 10,
    "KD Karthik": 141,
    "DJG Sammy": 12,
    "SA Abbott": 15,
    "MS Bisla": 37,
    "D Wiese": 127,
    "SN Khan": 177,
    "KW Richardson": 0,
    "Parvez Rasool": 8,
    "CJ Jordan": 1
}

// Plot Task 2


function sortProperties(obj, isNumericSort) {
    isNumericSort = isNumericSort || true; // by default numeric sort
    var sortable = [];
    for (var key in obj)
        if (obj.hasOwnProperty(key))
            sortable.push([key, obj[key]]);
    if (isNumericSort)
        sortable.sort(function (a, b) {
            return b[1] - a[1];
        });
    else
        sortable.sort(function (a, b) {
            var x = a[1].toLowerCase(),
                y = b[1].toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
    return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
}
// Sort Objects and outputs Objects

function sortObjects(objects) {

    var newObject = {};

    var sortedArray = sortProperties(objects, 'zindex', true, false);
    for (var i = 0; i < sortedArray.length; i++) {
        var key = sortedArray[i][0];
        var value = sortedArray[i][1];

        newObject[key] = value;

    }

    return newObject;

}

function inputChange() {
    var displayPlayers = document.getElementById('input-container')[0].value
    visualizeData2(dataRCB);
    return displayPlayers
}


function visualizeData2(data = dataRCB) {
    /* displayPlayers = inputChange() */
    displayPlayers = document.getElementById('input-container')[0].value

    if (displayPlayers === "" || displayPlayers < 1 || displayPlayers > 99) {
        displayPlayers = 10;
    }

    data = sortObjects(data);
    data = Object.assign( // collect all objects into a single obj
        ...Object // spread the final array as parameters
        .entries(data) // key a list of key/ value pairs
        .slice(0, displayPlayers) // get first n items of array
        .map(([k, v]) => ({
            [k]: v
        })) // map an object with a destructured key/value pair
    );

    // Create the chart

    Highcharts.chart('container2', {
        chart: {
            type: 'column',
        },
        title: {
            text: `Top ${displayPlayers} RCB Batsmen by Runs`
        },
        xAxis: {
            categories: Object.keys(data),
            title: {
                text: 'Player'
            }
        },
        yAxis: {
            title: {
                text: 'Total Runs Scored'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> runs<br/>'
        },

        series: [{
            data: Object.values(data)

        }]
    });

}

visualizeData2(dataRCB);

// Plot Task 3


function fetchAndVisualizeData3() {
    fetch(`json/task3.json`)
        .then(r => r.json())
        .then(data => {
            visualizeData3(data);
        })
}



function visualizeData3(data) {


    // Create the chart
    Highcharts.chart('container3', {
        chart: {
            type: 'column',
        },
        title: {
            text: 'Countrywise Foreign Umpires Count in IPL'
        },
        xAxis: {
            categories: Object.keys(data),
            title: {
                text: 'Country'
            }
        },
        yAxis: {
            title: {
                text: 'Number of Umpires'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> matches<br/>'
        },

        series: [{
            data: Object.values(data)

        }]
    });

}



fetchAndVisualizeData3();












// Plot Task 4


function fetchAndVisualizeData4() {
    fetch(`json/task4.json`)
        .then(r => r.json())
        .then(data => {
            visualizeData4(data);
        })
}



function visualizeData4(data) {

    const teams = ['KTK', 'DD', 'DC', 'RPS', 'KXIP', 'RR', 'GL', 'MI', 'RCB', 'KKR', 'CSK', 'SRH', 'PWI'];
    var megaStack = [];


    for (year = 2008; year <= 2017; year++) {
        var yearlySeries = {};
        var temp = []
        for (index = 0; index < teams.length; index++) {
            if (data[year][teams[index]] == undefined) {
                temp.push(0)
            } else {
                temp.push(data[year][teams[index]])
            }

        }
        yearlySeries['name'] = year;
        yearlySeries['data'] = temp;
        megaStack.push(yearlySeries);
    };
    // console.log(megaStack);
    Highcharts.chart('container4', {
        chart: {
            type: 'column',
        },
        title: {
            text: 'Seasonwise Matches Count For IPL Teams'
        },
        xAxis: {
            categories: teams,
            title: {
                text: 'Team'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches Count'
            },
            reversedStacks: false,
            stackLabels: {
                enabled: true,
                style: {

                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            enabled: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> matches<br/>'
        },

        series: megaStack
    });
}

fetchAndVisualizeData4();