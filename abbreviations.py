"""
This file contains all team abbreviations.
Used for plotting barplot without congesting the x-axis.
"""

abbreviations = {
    'Kochi Tuskers Kerala': 'KTK',
    'Delhi Daredevils': 'DD',
    'Deccan Chargers': 'DC',
    'Rising Pune Supergiant': 'RPS',
    'Kings XI Punjab': 'KXIP',
    'Rajasthan Royals': 'RR',
    'Rising Pune Supergiants': 'RPS',
    'Gujarat Lions': 'GL',
    'Mumbai Indians': 'MI',
    'Royal Challengers Bangalore': 'RCB',
    'Kolkata Knight Riders': 'KKR',
    'Chennai Super Kings': 'CSK',
    'Sunrisers Hyderabad': 'SRH',
    'Pune Warriors': 'PWI'
    }
