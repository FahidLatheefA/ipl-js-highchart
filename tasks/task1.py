"""
This module contains create_json_task1() function.
This function is used to create the task 1 json file
"""

##############################
# Importing Required Packages
##############################

import json
from abbreviations import abbreviations as abb

#########################################
# Task 1: Plot total runs scored by team
#########################################


def create_json_task1(deliveries_rows, deliveries_col_idx):
    """Function to create task1 json file
    """

    # Finding all distinct teams using sets
    teamlist = set()
    for row in deliveries_rows:
        teamlist.add(row[deliveries_col_idx['batting_team']])
    # print(teamlist)
    # We can see that 2 same franchise (renamed teams)
    # Rising Pune Supergiant and Rising Pune Supergiants
    # Both will be given the same abbreviation RPS

    # Adding runs dictionary
    runs_dict = {}
    for row in deliveries_rows:
        team = row[deliveries_col_idx['batting_team']]  # Team name
        team_code = abb[team]  # Team Code
        runs = row[deliveries_col_idx['total_runs']]  # Total runs

        # Creating runs database
        if team_code not in runs_dict:
            runs_dict[team_code] = 0  # Initialize team_code
        else:
            runs_dict[team_code] += int(runs)  # Adding runs
    with open("json/task1.json", "w") as write_file:
        json.dump(runs_dict, write_file, indent=4)  # Writing to json file
