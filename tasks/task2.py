"""
This module contains create_json_task2() function.
This function is used to create the task 2 json file
"""

##############################
# Importing Required Packages
##############################

import json

##################################
# Task 2: RCB top batsmen by runs
##################################


def create_json_task2(deliveries_rows, deliveries_col_idx):
    """Function to create task2 json file
    """

    # Adding Royal Challengers Bangalore players dictionary with runs
    # Using batsman_runs feature
    rcb_players_dict = {}
    for row in deliveries_rows:
        team = row[deliveries_col_idx['batting_team']]  # Team name
        if team == 'Royal Challengers Bangalore':
            # Creating RCB player runs database
            player = row[deliveries_col_idx['batsman']]  # Player name
            runs = row[deliveries_col_idx['batsman_runs']]  # Batsman runs

            if player not in rcb_players_dict:
                rcb_players_dict[player] = 0  # Initialize player
            else:
                rcb_players_dict[player] += int(runs)  # Adding runs
    with open("json/task2.json", "w") as write_file:
        json.dump(rcb_players_dict, write_file, indent=4)  # Writing to json file
