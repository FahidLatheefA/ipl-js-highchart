"""
This module contains create_json_task2() function.
This function is used to create the task 2 json file
"""

##############################
# Importing Required Packages
##############################

import json
from umpire_countries import umpire_dict

#################################################
# Task 3 : Umpires by Countries other than India
#################################################


def create_json_task3(matches_rows, matches_col_idx):
    """Function to create task3 json file
    """
    # List of all umpires
    umpire_set = set()
    for row in matches_rows:
        umpire1 = row[matches_col_idx['umpire1']].strip()  # Umpire 1 name
        umpire2 = row[matches_col_idx['umpire2']].strip()  # Umpire 2 name

        # Adding Umpires to the umpire_set
        if umpire1 != "":  # Non-empty string
            umpire_set.add(umpire1)

        if umpire2 != "":  # Non-empty string
            umpire_set.add(umpire2)

    # print(umpire_set)
    # print(len(umpire_set))  # Total 54 Umpires
    umpire_list = list(umpire_set)

    countrywise_umpire_count = {}
    for umpire in umpire_list:
        country = umpire_dict[umpire]
        if country == 'India':
            continue

        if country not in countrywise_umpire_count:
            countrywise_umpire_count[country] = 1  # Initialize
        else:
            countrywise_umpire_count[country] += 1  # Increment the count
    with open("json/task3.json", "w") as write_file:
        json.dump(countrywise_umpire_count, write_file, indent=4)  # Writing to json file
