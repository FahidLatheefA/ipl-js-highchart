"""
This module contains create_json_task4() function.
This function is used to create the task 4 json file
"""

##############################
# Importing Required Packages
##############################

import json
from abbreviations import abbreviations as abb

############################################################
# Task 4: Stacked chart of matches played by team by season
############################################################

def create_json_task4(matches_rows, matches_col_idx):
    """Function to create task4 json file
    """
    seasons = ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
    # Initialize mega database with team acronym
    mega_db = {}
    for season in seasons:
        mega_db[season] = {}  # Initialize with empty dictionary of season
    # print(mega_db)

    # Updating the database
    for row in matches_rows:
        team1 = row[matches_col_idx['team1']]  # Team1
        team1 = abb[team1]  # Using Team Acronyms
        team2 = row[matches_col_idx['team2']]  # Team2
        team2 = abb[team2]  # Using Acronyms
        season = row[matches_col_idx['season']]  # Season year

        # Updating Team 1
        if team1 not in mega_db[season]:
            mega_db[season].update({team1: 1})  # Initializing matchcount
        else:
            mega_db[season][team1] += 1  # Incrementing matchcount

        # Updating Team 2
        if team2 not in mega_db[season]:
            mega_db[season].update({team2: 1})  # Initializing matchcount
        else:
            mega_db[season][team2] += 1  # Incrementing matchcount
    with open("json/task4.json", "w") as write_file:
        json.dump(mega_db, write_file, indent=4)  # Writing to json file
